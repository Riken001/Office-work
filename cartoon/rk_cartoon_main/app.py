import os
import io
import uuid
import sys
import yaml
import traceback

with open('./rk_cartoon_main/config.yaml', 'r') as fd:
    opts = yaml.safe_load(fd)

sys.path.insert(0, './white_box_cartoonizer/')

import cv2
from flask import Flask, render_template, make_response, flash
import flask
from PIL import Image
import numpy as np
import skvideo.io
if opts['colab-mode']:
    from flask_ngrok import run_with_ngrok #to run the application on colab using ngrok


from cartoonize import WB_Cartoonize

if not opts['run_local']:
    if 'GOOGLE_APPLICATION_CREDENTIALS' in os.environ:
        from gcloud_utils import upload_blob, generate_signed_url, delete_blob, download_video
    else:
        raise Exception("GOOGLE_APPLICATION_CREDENTIALS not set in environment variables")
    from video_api import api_request
    # Algorithmia (GPU inference)
    import Algorithmia

app = Flask(__name__)
if opts['colab-mode']:
    run_with_ngrok(app)   #starts ngrok wh  en the app is run

app.config['UPLOAD_FOLDER_VIDEOS'] = 'static/uploaded_videos'
app.config['CARTOONIZED_FOLDER'] = 'static/cartoonized_images'

app.config['OPTS'] = opts

## Init Cartoonizer and load its weights 
wb_cartoonizer = WB_Cartoonize(os.path.abspath("white_box_cartoonizer/saved_models/"), ['gpu'])

def convert_bytes_to_image(img_bytes):
    
    pil_image = Image.open(io.BytesIO(img_bytes))
    if pil_image.mode=="RGBA":
        image = Image.new("RGB", pil_image.size, (255,255,255))
        image.paste(pil_image, mask=pil_image.split()[3])
    else:
        image = pil_image.convert('RGB')
    
    image = np.array(image)
    
    return image

import cv2

def cartoonize(img_path):
    img = flask.request.files[img_path].read()
    # img = cv2.imread(img_path)
    
    ## Read Image and convert to PIL (RGB) if RGBA convert appropriately
    image = convert_bytes_to_image(img)

    img_name = str(uuid.uuid4())
    
    cartoon_image = wb_cartoonizer.infer(image)
    
    cartoonized_img_name = os.path.join(['CARTOONIZED_FOLDER'], img_name + ".jpg")
    cv2.imwrite(cartoonized_img_name, cv2.cvtColor(cartoon_image, cv2.COLOR_RGB2BGR))
    
    # Upload to bucket
    output_uri = upload_blob("cartoonized_images", cartoonized_img_name, img_name + ".jpg", content_type='image/jpg')

    # Delete locally stored cartoonized image
    os.system("rm " + cartoonized_img_name)
    cartoonized_img_name = generate_signed_url(output_uri)
    cv2.imshow(cartoonized_img_name)
        


    return cartoonized_img_name

    
cartoonize('/home/eu4/HTML_CSS_JS_UDMY_larn/learning/flask/rk_cartoon_main/my.png')


